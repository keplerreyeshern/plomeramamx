import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ReleaseService {

  url = environment.baseUrl + '/api/release';

  constructor(private http: HttpClient) {
  }

  getRelease(release: string){
    return this.http.get<any>(this.url + '/' + release);
  }
}
