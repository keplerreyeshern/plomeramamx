import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  url = environment.baseUrl + '/api/contact';

  constructor(private http: HttpClient) {
  }

  contact(params: any){
    return this.http.post<any>(this.url, params);
  }
}
