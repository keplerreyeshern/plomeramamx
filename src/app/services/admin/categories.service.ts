import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  url = environment.baseUrl + '/api/categories';
  access_token = <string>sessionStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getCategories(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }


  showCategory(id: number){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }

  getCategoriesChildren(id: any){
    return this.http.get<any>(this.url + '/children/' + id, {headers: this.headers});
  }

  parentCategory(id: number){
    return this.http.get<any>(this.url + '/parent/' + id, {headers: this.headers});
  }

  activeCategories(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postCategories(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putCategories(id: any, params: any){
    return this.http.post<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteCategories(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
