import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  public language = sessionStorage.getItem('language');

  constructor() { }

  setLang(lang: string){
    this.language = lang;
  }

  getLang(){
    return this.language;
  }
}
