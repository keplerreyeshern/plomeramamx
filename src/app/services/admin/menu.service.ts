import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  public open = false;

  constructor() { }

  setOpen(action: boolean){
    this.open = action;
  }

  getOpen(){
    return this.open;
  }
}
