import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  url = environment.baseUrl + '/api/news';
  access_token = <string>sessionStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getNews(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }


  showNews(id: number){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }


  activeNews(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postNews(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putNews(id: any, params: any){
    return this.http.post<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteNews(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
