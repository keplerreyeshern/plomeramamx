import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogsService {

  url = environment.baseUrl  + '/api/blogs';
  access_token = <string>sessionStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getBlogs(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }


  showBlog(id: number){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }


  activeBlogs(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postBlogs(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putBlogs(id: any, params: any){
    return this.http.post<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteBlogs(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
