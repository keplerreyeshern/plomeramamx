import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url = environment.baseUrl + '/api/products';
  access_token = <string>sessionStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getProducts(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  getCategories(){
    return this.http.get<any>(this.url + '/create', {headers: this.headers});
  }

  getCategoriesChild(id: any){
    return this.http.get<any>(this.url + '/children/' + id, {headers: this.headers});
  }


  showProduct(id: number){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }

  parentCategory(id: number){
    return this.http.get<any>(this.url + '/parent/' + id, {headers: this.headers});
  }

  activeProducts(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postProducts(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putProducts(id: any, params: any){
    return this.http.post<any>(this.url + '/update/' + id, params, {headers: this.headers});
  }

  deleteProducts(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
