import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NavbarComponent} from './admin/navbar/navbar.component';
import {SidebarComponent} from './admin/sidebar/sidebar.component';
import { FooterComponent } from './admin/footer/footer.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { TranslationComponent } from './admin/translation/translation.component';
import {RouterModule} from '@angular/router';
import { NavbarClientComponent } from './public/navbar-client/navbar-client.component';
import { FooterClientComponent } from './public/footer-client/footer-client.component';
import {TranslateModule} from '@ngx-translate/core';



@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    TranslationComponent,
    NavbarClientComponent,
    FooterClientComponent,
  ],
  exports: [
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    TranslationComponent,
    NavbarClientComponent,
    FooterClientComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    TranslateModule
  ]
})
export class ComponentsModule { }
