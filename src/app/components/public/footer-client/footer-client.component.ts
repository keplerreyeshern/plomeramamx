import { Component, OnInit } from '@angular/core';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import {ComponentsService} from "../../../services/public/components.service";

@Component({
  selector: 'app-footer-client',
  templateUrl: './footer-client.component.html',
  styleUrls: ['./footer-client.component.sass']
})
export class FooterClientComponent implements OnInit {

  faPhone = faPhoneAlt;

  constructor(private service: ComponentsService) {

  }

  ngOnInit(): void {
  }

  clickTel(type: string){
    this.service.setClick(type).subscribe( response => {
      console.log(response);
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }



}
