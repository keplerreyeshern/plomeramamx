import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { faBars, faTimes, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import {ComponentsService} from "../../../services/public/components.service";

@Component({
  selector: 'app-navbar-client',
  templateUrl: './navbar-client.component.html',
  styleUrls: ['./navbar-client.component.sass']
})
export class NavbarClientComponent implements OnInit {

  //@ts-ignore
  @ViewChild('checkbox') checkbox: ElementRef;

  faTimes = faTimes;
  faBars = faBars;
  faPhoneAlt = faPhoneAlt;

  constructor(private service: ComponentsService) { }

  ngAfterViewInit() {
    console.log(this.checkbox.nativeElement.value);
  }

  ngOnInit(): void {
  }

  checkValue(){
    console.log(this.checkbox.nativeElement.checked);
  }

  goto(){
    if(this.checkbox.nativeElement.checked){
      this.checkbox.nativeElement.checked = false;
    }
  }

  clickTel(type: string){
    this.service.setClick(type).subscribe( response => {
      console.log(response);
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
