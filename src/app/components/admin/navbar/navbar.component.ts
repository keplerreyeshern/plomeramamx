import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth.service';
import {MenuService} from '../../../services/admin/menu.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  user = {
    name: 'Anonimo',
  };

  constructor(private router: Router,
              private authService: AuthService,
              public serviceMenu: MenuService) { }

  ngOnInit(): void {
    this.getData(sessionStorage.getItem('access_token'));
  }

  signout(){
    this.authService.signOut();
    this.router.navigate(['/login']);
  }

  getData(params: any){
    this.authService.getDataUser(params).subscribe( response => {
      this.user = response;
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  toggleMenu(){
    if (this.serviceMenu.getOpen()){
      this.serviceMenu.setOpen(false);
    } else {
      this.serviceMenu.setOpen(true);
    }
  }

}
