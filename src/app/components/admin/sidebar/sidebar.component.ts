import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { faTachometerAlt, faStore, faWarehouse, faUser, faImages, faNewspaper, faBlog } from '@fortawesome/free-solid-svg-icons';
import {MenuService} from '../../../services/admin/menu.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {


  faTachometerAlt = faTachometerAlt;
  faNewspaper = faNewspaper;
  faBlog = faBlog;
  faStore = faStore;
  faWarehouse = faWarehouse;
  faUser = faUser;
  faImages = faImages;

  constructor(public service: MenuService) {
  }

  ngOnInit(): void {
  }

  closeMenu(){
    this.service.setOpen(false);
  }
}
