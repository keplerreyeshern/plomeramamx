import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'PlomeramaMX';

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('es');
    translate.use('es');
  }


  switchLanguage(language: string) {
    this.translate.use(language);
  }
}
