import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PasswordRoutingModule } from './password-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PasswordComponent } from './password.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import {EmailComponent} from './email/email.component';
import {FindComponent} from './find/find.component';
import {FormsModule} from '@angular/forms';

export function TranslationLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}



@NgModule({
  imports: [
    CommonModule,
    PasswordRoutingModule,
    NgxSpinnerModule,
    ComponentsModule,
    TranslateModule.forRoot({
      loader: {provide: TranslateLoader, useFactory: TranslationLoaderFactory, deps: [HttpClient]}
    }),
    FormsModule
  ],
  declarations: [PasswordComponent, EmailComponent, FindComponent]
})
export class PasswordModule { }
