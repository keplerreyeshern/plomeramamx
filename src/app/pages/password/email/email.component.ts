import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {PasswordService} from '../../../services/public/password.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.sass']
})
export class EmailComponent implements OnInit {

  message = '';
  alert = false;

  constructor(private loading: NgxSpinnerService,
              private service: PasswordService) { }

  ngOnInit(): void {
  }

  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('email', form.value.email);
    this.service.createPassword(params).subscribe( response => {
      this.message = response.message;
      this.alert = true;
      form.resetForm();
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        this.loading.hide();
      } else if(err.status == 404){
        this.message = err.error.message;
        this.alert = true;
        form.resetForm();
        this.loading.hide();
      } else {
        alert('se detecto un error comunicate con el administrador');
        this.loading.hide();
      }
    });
  }

}
