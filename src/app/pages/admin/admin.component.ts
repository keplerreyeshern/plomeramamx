import {Component, OnInit} from '@angular/core';
import {TranslationService} from '../../services/admin/translation.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  public language = sessionStorage.getItem('language');


  constructor(public serviceTranslation: TranslationService,
              private router: Router) {
    // if(!sessionStorage.getItem('access_token')){
    //   this.router.navigateByUrl('/login');
    // }
  }

  ngOnInit(): void {

  }

}
