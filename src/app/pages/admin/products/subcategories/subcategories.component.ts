import { Component, OnInit, forwardRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import {ProductsService} from '../../../../services/admin/products.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {TranslationService} from '../../../../services/admin/translation.service';

@Component({
  selector: 'app-subcategories',
  templateUrl: './subcategories.component.html',
  styleUrls: ['./subcategories.component.sass']
})
export class SubcategoriesComponent implements OnInit {

  @Input()
  parent!: string;
  @Input() categoriesSelect:any[]=[];
  // @ts-ignore
  @ViewChild(SubcategoriesComponent) SubCategories: SubcategoriesComponent;

  @Output() categorySelect: EventEmitter<string>;

  categories:any[]=[];


  constructor(private service: ProductsService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService) {
    this.categorySelect = new EventEmitter();
  }

  ngOnInit(): void {
    this.getData();
  }

  push(category:any){
    // @ts-ignore
    this.categorySelect.emit(category);
  }

  activeCategories(){
    for(let p=0; p<this.categoriesSelect.length; p++){
      for (let l=0; l<this.categories.length; l++){
        if(this.categories[l].id == this.categoriesSelect[p].id){
          this.categories[l].status = true;
        }
      }
    }
  }

  getData(){
    this.loading.show();
    this.service.getCategoriesChild(this.parent).subscribe(response => {
      this.categories = response;
      this.activeCategories();
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
