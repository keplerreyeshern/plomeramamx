import { Component, OnInit } from '@angular/core';
import { faList, faStore, faSave, faEye, faEyeSlash, faImages } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {TranslationService} from '../../../../services/admin/translation.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {ProductsService} from '../../../../services/admin/products.service';

@Component({
  selector: 'app-create-products',
  templateUrl: './create-products.component.html',
  styleUrls: ['./create-products.component.sass']
})
export class CreateProductsComponent implements OnInit {

  faList = faList;
  faImages = faImages;
  faStore = faStore;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = sessionStorage.getItem('language');
  url_images = sessionStorage.getItem('url_images');
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  description:any;
  editImage = true;
  categories:any[]=[];
  categoriesSelect:any[]=[];


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private router: Router) {
    this.getCategories();
  }

  ngOnInit(): void {
  }

  push(category: any){
    let verify = this.categoriesSelect.filter(item => item === category);
    if (verify.length == 0){
      this.categoriesSelect.push(category);
      // console.log('no esta');
    } else {
      let index = this.categoriesSelect.findIndex(item => item === category);
      this.categoriesSelect.splice(index, 1);
      // console.log('si esta');
    }
    // console.log(this.categoriesSelect);
  }

  getCategories(){
    this.loading.show();
    this.service.getCategories().subscribe(response => {
      this.categories = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('model', form.value.model);
    params.append('key', form.value.key);
    params.append('description', form.value.description);
    params.append('details', form.value.details);
    if (this.image){
      params.append('image', this.image);
    }
    // @ts-ignore
    params.append('categories', this.categoriesSelect);
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.postProducts(params).subscribe( response => {
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/products');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
