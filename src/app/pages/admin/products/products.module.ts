import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComponentsModule} from '../../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import {ProductsRoutingModule} from './products-routing.module';
import { ListProductsComponent } from './list-products/list-products.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgxPaginationModule} from 'ngx-pagination';
import { CreateProductsComponent } from './create-products/create-products.component';
import { EditProductsComponent } from './edit-products/edit-products.component';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {FormsModule} from '@angular/forms';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import { SubcategoriesComponent } from './subcategories/subcategories.component';



@NgModule({
  declarations: [ListProductsComponent, CreateProductsComponent, EditProductsComponent, SubcategoriesComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    ProductsRoutingModule,
    FontAwesomeModule,
    NgxPaginationModule,
    TranslateModule.forRoot(),
    NgxDropzoneModule,
    FormsModule,
    CKEditorModule
  ]
})
export class ProductsModule { }
