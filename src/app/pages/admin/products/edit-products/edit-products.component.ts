import { Component, OnInit } from '@angular/core';
import { faList, faStore, faEdit, faEye, faEyeSlash, faImages, faTrashAlt, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {TranslationService} from '../../../../services/admin/translation.service';
import {ProductsService} from '../../../../services/admin/products.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {GalleriesService} from '../../../../services/admin/galleries.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-edit-products',
  templateUrl: './edit-products.component.html',
  styleUrls: ['./edit-products.component.sass']
})
export class EditProductsComponent implements OnInit {

  faList = faList;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faImages = faImages;
  faStore = faStore;
  faEdit = faEdit;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = sessionStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  product = {
    name: {
      es: '',
      en: ''
    },
    model: '',
    key: '',
    description: {
      es: '',
      en: ''
    },
    details: {
      es: '',
      en: ''
    },
  };
  images:any[]=[];
  gallery: any;
  description:any;
  editImage = true;
  categories:any[]=[];
  categoriesSelect:any[]=[];
  categoriesSelect1:any[]=[];
  categoriesSelect2:any[]=[];


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: ProductsService,
              private loading: NgxSpinnerService,
              private router: Router,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getCategories();
    this.activatedRoute.params.subscribe( params => {
      this.id = params['product'];
      this.getProducts(params['product']);
    });
  }

  push(category: any){
    let verify = this.categoriesSelect.filter(item => item === category);
    if (verify.length == 0){
      this.categoriesSelect.push(category);
      // console.log('no esta');
    } else {
      let index = this.categoriesSelect.findIndex(item => item === category);
      this.categoriesSelect.splice(index, 1);
      // console.log('si esta');
    }
    // console.log(this.categoriesSelect);
  }

  getCategories(){
    this.loading.show();
    this.service.getCategories().subscribe(response => {
      this.categories = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getProducts(id: number){
    this.loading.show();
    this.service.showProduct(id).subscribe( response => {
      this.product = response.product;
      this.imageInit = response.product.image;
      this.images = response.images;
      this.gallery = response.gallery;
      this.categoriesSelect1 = response.categories;
      console.log(this.categoriesSelect1);
      console.log(response.categories);
      this.activeCategories();
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  activeCategories(){
    for(let i=0; i<this.categoriesSelect1.length; i++){
      for (let s=0; s<this.categories.length; s++){
        if(this.categories[s].id == this.categoriesSelect1[i].id){
          this.categories[s].status = true;
        }
      }
    }
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('model', form.value.model);
    params.append('key', form.value.key);
    params.append('description', form.value.description);
    params.append('details', form.value.details);
    if (this.image){
      params.append('image', this.image);
    }
    // @ts-ignore
    params.append('categories', this.categoriesSelect);
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.putProducts(this.id, params).subscribe( response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/products');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  deleteImage(gallery: number){
    this.loading.show();
    this.serviceImage.deleteImage(gallery).subscribe( response => {
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == response.id);
      this.images.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  activeImage(gallery: number){
    this.loading.show();
    this.serviceImage.activeImage(gallery).subscribe( response => {
      const user = response;
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == user.id);
      this.images[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
