import { Component, OnInit } from '@angular/core';
import { faPlus, faStore } from '@fortawesome/free-solid-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import {TranslationService} from '../../../../services/admin/translation.service';
import {ProductsService} from '../../../../services/admin/products.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.sass']
})
export class ListProductsComponent implements OnInit {

  faPlus = faPlus;
  faStore = faStore;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.baseUrl;
  products:any[]=[];
  productsAll:any[]=[];
  public page: number | undefined;

  constructor(private service: ProductsService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService,) {
    this.getData();
  }

  ngOnInit(): void {
  }

  getData(){
    this.loading.show();
    this.service.getProducts().subscribe( response => {
      this.products = response;
      this.productsAll = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(category: number){
    this.loading.show();
    this.service.activeProducts(category).subscribe( response => {
      const user = response;
      const index = this.products.findIndex(item => item.id == user.id);
      this.products[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(category: number){
    this.loading.show();
    this.service.deleteProducts(category).subscribe( response => {
      const index = this.products.findIndex(item => item.id == response.id);
      this.products.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getParentEs(parent: number){
    let category = this.productsAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.es;
    } else {
      return '';
    }
  }
  getParentEn(parent: number){
    let category = this.productsAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.en;
    } else {
      return '';
    }
  }

  filter(search: string){
    let regex = new RegExp(search, 'i');
    if (sessionStorage.getItem('language') == 'es'){
      this.products = this.productsAll.filter(item => regex.test(item.name.es));
    } else {
      this.products = this.productsAll.filter(item => regex.test(item.name.en));
    }
  }

}
