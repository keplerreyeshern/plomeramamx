import { Component, OnInit } from '@angular/core';
import { faPlus, faImages } from '@fortawesome/free-solid-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import {GalleriesService} from '../../../../services/admin/galleries.service';
import {TranslationService} from '../../../../services/admin/translation.service';

@Component({
  selector: 'app-list-galleries',
  templateUrl: './list-galleries.component.html',
  styleUrls: ['./list-galleries.component.sass']
})
export class ListGalleriesComponent implements OnInit {

  faPlus = faPlus;
  faImages = faImages;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  galleries:any[]=[];
  galleriesAll:any[]=[];
  public page: number | undefined;

  constructor(private service: GalleriesService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService,) {
    this.getData();
  }

  ngOnInit(): void {
  }

  getData(){
    this.loading.show();
    this.service.getGalleries().subscribe( response => {
      this.galleries = response;
      this.galleriesAll = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(gallery: number){
    this.loading.show();
    this.service.activeGalleries(gallery).subscribe( response => {
      const user = response;
      const index = this.galleries.findIndex(item => item.id == user.id);
      this.galleries[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(gallery: number){
    this.loading.show();
    this.service.deleteGalleries(gallery).subscribe( response => {
      const index = this.galleries.findIndex(item => item.id == response.id);
      this.galleries.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  filter(search: string){
    let regex = new RegExp(search, 'i');
    if (sessionStorage.getItem('language') == 'es'){
      this.galleries = this.galleriesAll.filter(item => regex.test(item.name.es));
    } else {
      this.galleries = this.galleriesAll.filter(item => regex.test(item.name.en));
    }
  }

}
