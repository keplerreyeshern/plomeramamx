import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComponentsModule} from '../../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import {GalleriesRoutingModule} from './galleries-routing.module';
import { ListGalleriesComponent } from './list-galleries/list-galleries.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { CreateGalleriesComponent } from './create-galleries/create-galleries.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {FormsModule} from '@angular/forms';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import { EditGalleriesComponent } from './edit-galleries/edit-galleries.component';
import {NgxPaginationModule} from 'ngx-pagination';



@NgModule({
  declarations: [ListGalleriesComponent, CreateGalleriesComponent, EditGalleriesComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    FontAwesomeModule,
    FormsModule,
    CKEditorModule,
    NgxDropzoneModule,
    NgxPaginationModule,
    GalleriesRoutingModule,
    TranslateModule.forRoot()
  ]
})
export class GalleriesModule { }
