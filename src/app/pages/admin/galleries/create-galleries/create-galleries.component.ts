import { Component, OnInit } from '@angular/core';
import { faList, faImages, faSave, faEye, faEyeSlash  } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {TranslationService} from '../../../../services/admin/translation.service';
import {GalleriesService} from '../../../../services/admin/galleries.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-galleries',
  templateUrl: './create-galleries.component.html',
  styleUrls: ['./create-galleries.component.sass']
})
export class CreateGalleriesComponent implements OnInit {

  faList = faList;
  faImages = faImages;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = sessionStorage.getItem('language');
  public Editor = ClassicEditor;


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: GalleriesService,
              private loading: NgxSpinnerService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('language', language);
    params.append('fileslength', filesLength);
    params.append('description', form.value.description);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.postGalleries(params).subscribe( response => {
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/galleries');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
