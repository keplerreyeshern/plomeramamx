import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GalleriesComponent} from './galleries.component';
import {ListGalleriesComponent} from './list-galleries/list-galleries.component';
import {CreateGalleriesComponent} from './create-galleries/create-galleries.component';
import {EditGalleriesComponent} from './edit-galleries/edit-galleries.component';

const routes: Routes = [
  {
    path: '',
    component: GalleriesComponent,
    children: [
      {
        path: 'list',
        component: ListGalleriesComponent
      },
      {
        path: 'create',
        component: CreateGalleriesComponent
      },
      {
        path: 'edit/:gallery',
        component: EditGalleriesComponent
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GalleriesRoutingModule { }
