import { Component, OnInit } from '@angular/core';
import {TranslationService} from '../../../../services/admin/translation.service';
import {GalleriesService} from '../../../../services/admin/galleries.service';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { faList, faImages, faEdit, faTrashAlt, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-edit-galleries',
  templateUrl: './edit-galleries.component.html',
  styleUrls: ['./edit-galleries.component.sass']
})
export class EditGalleriesComponent implements OnInit {

  faList = faList;
  faImages = faImages;
  faEdit = faEdit;
  faTrashAlt = faTrashAlt;
  faPowerOff = faPowerOff;
  id: number | undefined;
  gallery = {
    name: {
      es: 'edit',
      en: 'edit'
    }
  };
  url_images = environment.baseUrl;
  images: any;
  language = sessionStorage.getItem('language');
  public Editor = ClassicEditor;


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: GalleriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.id = params['gallery'];
      this.getGallery(params['gallery']);
    });
  }

  ngOnInit(): void {
  }

  getGallery(id: number){
    this.loading.show();
    this.service.showGallery(id).subscribe( response => {
      // console.log(response);
      this.gallery = response.gallery;
      this.images = response.images;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 404){
        this.loading.hide();
        alert('El usuario que desea buscar no existé verifique e intente nuevamente');
        this.router.navigateByUrl('/admin/users');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    params.append('language', language);
    params.append('fileslength', filesLength);
    params.append('description', form.value.description);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.putGalleries(this.id, params).subscribe( response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/galleries');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  deleteImage(gallery: number){
    this.loading.show();
    this.service.deleteImage(gallery).subscribe( response => {
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == response.id);
      this.images.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  activeImage(gallery: number){
    this.loading.show();
    this.service.activeImage(gallery).subscribe( response => {
      const user = response;
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == user.id);
      this.images[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
