import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGalleriesComponent } from './edit-galleries.component';

describe('EditGalleriesComponent', () => {
  let component: EditGalleriesComponent;
  let fixture: ComponentFixture<EditGalleriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditGalleriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGalleriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
