import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NewsRoutingModule} from './news-routing.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ComponentsModule} from '../../../components/components.module';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {TranslateModule} from '@ngx-translate/core';
import { ListNewsComponent } from './list-news/list-news.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { CreateNewsComponent } from './create-news/create-news.component';
import { EditNewsComponent } from './edit-news/edit-news.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [ListNewsComponent, CreateNewsComponent, EditNewsComponent],
  imports: [
    CommonModule,
    NewsRoutingModule,
    NgxSpinnerModule,
    ComponentsModule,
    CKEditorModule,
    NgxPaginationModule,
    NgxDropzoneModule,
    TranslateModule.forRoot(),
    FontAwesomeModule,
    FormsModule
  ]
})
export class NewsModule { }
