import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewsComponent} from './news.component';
import {ListNewsComponent} from './list-news/list-news.component';
import {CreateNewsComponent} from './create-news/create-news.component';
import {EditNewsComponent} from './edit-news/edit-news.component';

const routes: Routes = [
  {
    path: '',
    component: NewsComponent,
    children: [
      {
        path: 'list',
        component: ListNewsComponent
      },
      {
        path: 'create',
        component: CreateNewsComponent
      },
      {
        path: 'edit/:news',
        component: EditNewsComponent
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsRoutingModule { }
