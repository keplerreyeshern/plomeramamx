import { Component, OnInit } from '@angular/core';
import { faPlus, faNewspaper } from '@fortawesome/free-solid-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import {TranslationService} from '../../../../services/admin/translation.service';
import {NewsService} from '../../../../services/admin/news.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-list-news',
  templateUrl: './list-news.component.html',
  styleUrls: ['./list-news.component.sass']
})
export class ListNewsComponent implements OnInit {

  faPlus = faPlus;
  faNewspaper = faNewspaper;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.baseUrl;
  news:any[]=[];
  newsAll:any[]=[];
  public page: number | undefined;

  constructor(private service: NewsService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService,) {
    this.getData();
  }

  ngOnInit(): void {
  }

  getData(){
    this.loading.show();
    this.service.getNews().subscribe( response => {
      this.news = response;
      this.news.reverse();
      this.newsAll = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(news: number){
    this.loading.show();
    this.service.activeNews(news).subscribe( response => {
      const user = response;
      const index = this.news.findIndex(item => item.id == user.id);
      this.news[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(news: number){
    this.loading.show();
    this.service.deleteNews(news).subscribe( response => {
      const index = this.news.findIndex(item => item.id == response.id);
      this.news.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getParentEs(parent: number){
    let category = this.newsAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.es;
    } else {
      return '';
    }
  }
  getParentEn(parent: number){
    let category = this.newsAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.en;
    } else {
      return '';
    }
  }

  filter(search: string){
    let regex = new RegExp(search, 'i');
    if (sessionStorage.getItem('language') == 'es'){
      this.news = this.newsAll.filter(item => regex.test(item.title.es));
    } else {
      this.news = this.newsAll.filter(item => regex.test(item.title.en));
    }
  }

}
