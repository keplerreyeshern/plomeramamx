import { Component, OnInit } from '@angular/core';
import { faList, faNewspaper, faSave, faEye, faEyeSlash, faImages } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {TranslationService} from '../../../../services/admin/translation.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {NewsService} from '../../../../services/admin/news.service';
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'app-create-news',
  templateUrl: './create-news.component.html',
  styleUrls: ['./create-news.component.sass']
})
export class CreateNewsComponent implements OnInit {

  faList = faList;
  faImages = faImages;
  faNewspaper = faNewspaper;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = environment.lenguage;
  url_images = sessionStorage.getItem('url_images');
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  description:any;
  editImage = true;
  imageName = '';


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: NewsService,
              private loading: NgxSpinnerService,
              private router: Router) {
  }

  ngOnInit(): void {
  }



  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('content', form.value.content);
    params.append('intro', form.value.intro);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.postNews(params).subscribe( response => {
      this.loading.hide();
      console.log(response);
      this.router.navigateByUrl('/admin/news');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.imageName = file.name;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
