import { Component, OnInit } from '@angular/core';
import { faPlus, faWarehouse } from '@fortawesome/free-solid-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import {TranslationService} from '../../../../services/admin/translation.service';
import {CategoriesService} from '../../../../services/admin/categories.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.sass']
})
export class ListCategoriesComponent implements OnInit {

  faPlus = faPlus;
  faWarehouse = faWarehouse;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.baseUrl;
  categories:any[]=[];
  categoriesAll:any[]=[];
  public page: number | undefined;

  constructor(private service: CategoriesService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService,) {
    this.getData();
  }

  ngOnInit(): void {
  }

  getData(){
    this.loading.show();
    this.service.getCategories().subscribe( response => {
      this.categoriesAll = response;
      this.categories = this.categoriesAll.filter(item => item.parent_id == null);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(category: number){
    this.loading.show();
    this.service.activeCategories(category).subscribe( response => {
      const user = response;
      const index = this.categories.findIndex(item => item.id == user.id);
      this.categories[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(category: number){
    this.loading.show();
    this.service.deleteCategories(category).subscribe( response => {
      const index = this.categories.findIndex(item => item.id == response.id);
      this.categories.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getParentEs(parent: number){
    let category = this.categoriesAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.es;
    } else {
      return '';
    }
  }
  getParentEn(parent: number){
    let category = this.categoriesAll.filter(item => item.id == parent);
    if(category.length != 0){
      return category[0].name.en;
    } else {
      return '';
    }
  }

  filter(search: string){
    let regex = new RegExp(search, 'i');
    if (sessionStorage.getItem('language') == 'es'){
      this.categories = this.categoriesAll.filter(item => regex.test(item.name.es));
    } else {
      this.categories = this.categoriesAll.filter(item => regex.test(item.name.en));
    }
  }

}
