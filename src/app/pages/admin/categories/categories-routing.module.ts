import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CategoriesComponent} from './categories.component';
import {ListCategoriesComponent} from './list-categories/list-categories.component';
import {CreateCategoriesComponent} from './create-categories/create-categories.component';
import {EditCategoriesComponent} from './edit-categories/edit-categories.component';
import {CreateSubcategoriesComponent} from './create-subcategories/create-subcategories.component';

const routes: Routes = [
  {
    path: '',
    component: CategoriesComponent,
    children: [
      {
        path: 'list',
        component: ListCategoriesComponent
      },
      {
        path: 'create',
        component: CreateCategoriesComponent
      },
      {
        path: 'subcategories/create/:parent',
        component: CreateSubcategoriesComponent
      },
      {
        path: 'edit/:category',
        component: EditCategoriesComponent
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriesRoutingModule { }
