import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComponentsModule} from '../../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import {CategoriesRoutingModule} from './categories-routing.module';
import { ListCategoriesComponent } from './list-categories/list-categories.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgxPaginationModule} from 'ngx-pagination';
import { EditCategoriesComponent } from './edit-categories/edit-categories.component';
import { CreateCategoriesComponent } from './create-categories/create-categories.component';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {FormsModule} from '@angular/forms';
import { CreateSubcategoriesComponent } from './create-subcategories/create-subcategories.component';
import { SubcateriesCategoriesComponent } from './subcateries-categories/subcateries-categories.component';



@NgModule({
  declarations: [ListCategoriesComponent, EditCategoriesComponent, CreateCategoriesComponent, CreateSubcategoriesComponent, SubcateriesCategoriesComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    FontAwesomeModule,
    CategoriesRoutingModule,
    NgxPaginationModule,
    TranslateModule.forRoot(),
    NgxDropzoneModule,
    FormsModule
  ]
})
export class CategoriesModule { }
