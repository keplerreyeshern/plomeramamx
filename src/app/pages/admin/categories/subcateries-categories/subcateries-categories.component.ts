import {Component, forwardRef, Input, OnInit, ViewChild} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {TranslationService} from '../../../../services/admin/translation.service';
import {CategoriesService} from '../../../../services/admin/categories.service';
import { faPlus, faWarehouse } from '@fortawesome/free-solid-svg-icons';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-subcateries-categories',
  templateUrl: './subcateries-categories.component.html',
  styleUrls: ['./subcateries-categories.component.sass']
})
export class SubcateriesCategoriesComponent implements OnInit {

  @Input()
  parent!: string;
  // @ts-ignore
  @ViewChild(SubcateriesCategoriesComponent) SubcateriesCategoriesComponent: SubcateriesCategoriesComponent;
  categories:any[]=[];
  faPlus = faPlus;
  faWarehouse = faWarehouse;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.baseUrl;

  constructor(private service : CategoriesService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getCategoriesChildren(this.parent).subscribe(response => {
      this.categories = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(category: number){
    this.loading.show();
    this.service.activeCategories(category).subscribe( response => {
      const user = response;
      const index = this.categories.findIndex(item => item.id == user.id);
      this.categories[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(category: number){
    this.loading.show();
    this.service.deleteCategories(category).subscribe( response => {
      const index = this.categories.findIndex(item => item.id == response.id);
      this.categories.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
