import { Component, OnInit } from '@angular/core';
import { faList, faWarehouse, faEdit, faEye, faEyeSlash, faImages, faTrashAlt, faPowerOff } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {TranslationService} from '../../../../services/admin/translation.service';
import {CategoriesService} from '../../../../services/admin/categories.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {GalleriesService} from '../../../../services/admin/galleries.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-edit-categories',
  templateUrl: './edit-categories.component.html',
  styleUrls: ['./edit-categories.component.sass']
})
export class EditCategoriesComponent implements OnInit {

  faList = faList;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faImages = faImages;
  faWarehouse = faWarehouse;
  faEdit = faEdit;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = sessionStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  name:any;
  category = {
    name: {
      es: '',
      en: ''
    }
  };
  images:any[]=[];
  gallery: any;
  description:any;
  editImage = true;


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: CategoriesService,
              private loading: NgxSpinnerService,
              private router: Router,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.id = params['category'];
      this.getCategory(params['category']);
    });
  }

  ngOnInit(): void {
  }


  getCategory(id: number){
    this.loading.show();
    this.service.showCategory(id).subscribe( response => {
      console.log(response);
      this.category = response.category;
      this.imageInit = response.category.image;
      this.images = response.images;
      this.gallery = response.gallery;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('name', form.value.name);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    if(this.gallery){
      params.append('gallery', this.gallery.id);
    } else {
      params.append('gallery', '');
    }
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.putCategories(this.id, params).subscribe( response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/categories');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  deleteImage(gallery: number){
    this.loading.show();
    this.serviceImage.deleteImage(gallery).subscribe( response => {
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == response.id);
      this.images.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  activeImage(gallery: number){
    this.loading.show();
    this.serviceImage.activeImage(gallery).subscribe( response => {
      const user = response;
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == user.id);
      this.images[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
