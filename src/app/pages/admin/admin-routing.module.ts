import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AdminComponent} from './admin.component';
import {AuthGuard} from '../../components/guards/gaurd/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'users',
        loadChildren: () => import('../admin/users/users.module').then( m => m.UsersModule)
      },
      // {
      //   path: 'categories',
      //   loadChildren: () => import('../admin/categories/categories.module').then( m => m.CategoriesModule)
      // },
      // {
      //   path: 'products',
      //   loadChildren: () => import('../admin/products/products.module').then( m => m.ProductsModule)
      // },
      // {
      //   path: 'galleries',
      //   loadChildren: () => import('../admin/galleries/galleries.module').then( m => m.GalleriesModule)
      // },
      // {
      //   path: 'blogs',
      //   loadChildren: () => import('../admin/blogs/blogs.module').then( m => m.BlogsModule)
      // },
      {
        path: 'news',
        loadChildren: () => import('../admin/news/news.module').then( m => m.NewsModule)
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
