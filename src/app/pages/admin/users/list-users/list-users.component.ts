import { Component, OnInit } from '@angular/core';
import { faPlus, faUsers } from '@fortawesome/free-solid-svg-icons';
import {UsersService} from '../../../../services/admin/users.service';
import {NgxSpinnerService} from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.sass']
})
export class ListUsersComponent implements OnInit {

  faPlus = faPlus;
  faUsers = faUsers;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  users:any[]=[];
  usersAll:any[]=[];
  public page: number | undefined;

  constructor(private service: UsersService,
              private loading: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.loading.show();
    this.service.getUsers().subscribe( response => {
      this.users = response;
      this.users.sort(function (a, b) {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
      this.usersAll = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(user: number){
    this.loading.show();
    this.service.activeUsers(user).subscribe( response => {
      const user = response;
      const index = this.users.findIndex(item => item.id == user.id);
      this.users[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(user: number){
    this.loading.show();
    this.service.deleteUsers(user).subscribe( response => {
      const index = this.users.findIndex(item => item.id == response.id);
      this.users.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  filter(search: string){
    let regex = new RegExp(search, 'i');
    this.users = this.usersAll.filter(item => regex.test(item.name));
  }

}
