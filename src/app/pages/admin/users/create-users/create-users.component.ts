import { Component, OnInit } from '@angular/core';
import { faList, faUsers, faSave, faEye, faEyeSlash  } from '@fortawesome/free-solid-svg-icons';
import {UsersService} from '../../../../services/admin/users.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-users',
  templateUrl: './create-users.component.html',
  styleUrls: ['./create-users.component.sass']
})
export class CreateUsersComponent implements OnInit {

  faList = faList;
  faUsers = faUsers;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  show = false;
  type = 'password';

  constructor(private service: UsersService,
              private loading: NgxSpinnerService,
              private router: Router) { }

  ngOnInit(): void {
  }

  submit(form: NgForm){
    if(form.value.password != form.value.pass){
      alert('Las contraseñas deben coincidir')
    } else {
      this.service.getUser(form.value.email).subscribe(res => {
        const user = res;
        if (user.length != 0){
          alert('El correo ya se encuentra registrado, intenta con otro')
        } else {
          this.loading.show();
          const params = {
            name: form.value.name,
            email: form.value.email,
            password: form.value.password,
          };
          this.service.postUsers(params).subscribe( response => {
            this.loading.hide();
            this.router.navigateByUrl('/admin/users');
          }, err => {
            if(err.status == 500){
              this.loading.hide();
              alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
            } else {
              this.loading.hide();
              alert('se detecto un error comunicate con el administrador');
            }
          });
        }
      }, err => {
        if(err.status == 500){
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
    }
  }

  showPassword(){
    if(!this.show){
      this.show = true;
      this.type = 'text';
    } else {
      this.show = false;
      this.type = 'password';
    }
  }


}
