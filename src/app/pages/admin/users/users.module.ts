import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComponentsModule} from '../../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import {UsersRoutingModule} from './users-routing.module';
import { ListUsersComponent } from './list-users/list-users.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { CreateUsersComponent } from './create-users/create-users.component';
import {FormsModule} from '@angular/forms';
import { EditUsersComponent } from './edit-users/edit-users.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
  declarations: [ListUsersComponent, CreateUsersComponent, EditUsersComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    FontAwesomeModule,
    UsersRoutingModule,
    NgxDropzoneModule,
    NgxPaginationModule,
    TranslateModule.forRoot(),
    FormsModule
  ]
})
export class UsersModule { }
