import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../../../services/admin/users.service';
import { faList, faUsers, faEdit, faEye, faEyeSlash  } from '@fortawesome/free-solid-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.sass']
})
export class EditUsersComponent implements OnInit {

  id: number | undefined;
  user = {
    name: '',
    email: ''
  };
  faList = faList;
  faUsers = faUsers;
  faEdit = faEdit;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  show = false;
  passwordEdit = false;
  type = 'password';

  constructor(private loading: NgxSpinnerService,
              private service: UsersService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.id = params['user'];
      this.getUser(params['user']);
    });
  }

  ngOnInit(): void {

  }

  getUser(id: number){
    this.loading.show();
    this.service.showUser(id).subscribe( response => {
      // console.log(response);
      this.user = response;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 404){
        this.loading.hide();
        alert('El usuario que desea buscar no existé verifique e intente nuevamente');
        this.router.navigateByUrl('/admin/users');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  submit(form: NgForm){
    if(form.value.password != form.value.pass){
      alert('Las contraseñas deben coincidir')
    } else {
      this.service.getUser(form.value.email).subscribe(res => {
        const user = res;
        if (!user.empty) {
          if (user.email == this.user.email){
            this.update(form);
          } else {
            alert('El correo ya se encuentra registrado, intenta con otro');
          }
        } else {
          this.update(form);
        }
      }, err => {
        if (err.status == 500) {
          this.loading.hide();
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          this.loading.hide();
          alert('se detecto un error comunicate con el administrador');
        }
      });
    }
  }

  update(form: NgForm){
    this.loading.show();
    let params: any;
    if (form.value.password_change){
      params = {
        name: form.value.name,
        email: form.value.email,
        password: form.value.password,
        checkbox: 1
      };
    } else {
      params = {
        name: form.value.name,
        email: form.value.email,
        password: form.value.password,
        checkbox: 0
      };
    }
    this.service.putUsers(this.id, params).subscribe(response => {
      this.loading.hide();
      this.router.navigateByUrl('/admin/users');
    }, err => {
      if (err.status == 500) {
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  showPassword(){
    if(!this.show){
      this.show = true;
      this.type = 'text';
    } else {
      this.show = false;
      this.type = 'password';
    }
  }

  editPassword(checkbox: any){
    if(!checkbox.checked){
      this.passwordEdit = false;
    } else {
      this.passwordEdit = true;
    }
  }

}
