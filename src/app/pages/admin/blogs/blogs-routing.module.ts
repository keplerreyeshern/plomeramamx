import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BlogsComponent} from './blogs.component';
import {ListBlogsComponent} from './list-blogs/list-blogs.component';
import {CreateBlogsComponent} from './create-blogs/create-blogs.component';
import {EditBlogsComponent} from './edit-blogs/edit-blogs.component';

const routes: Routes = [
  {
    path: '',
    component: BlogsComponent,
    children: [
      {
        path: 'list',
        component: ListBlogsComponent
      },
      {
        path: 'create',
        component: CreateBlogsComponent
      },
      {
        path: 'edit/:blog',
        component: EditBlogsComponent
      },
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlogsRoutingModule { }
