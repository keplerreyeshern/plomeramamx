import { Component, OnInit } from '@angular/core';
import { faPlus, faBlog } from '@fortawesome/free-solid-svg-icons';
import {NgxSpinnerService} from 'ngx-spinner';
import { faPowerOff, faTrashAlt,  } from '@fortawesome/free-solid-svg-icons';
import {TranslationService} from '../../../../services/admin/translation.service';
import {BlogsService} from '../../../../services/admin/blogs.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-list-blogs',
  templateUrl: './list-blogs.component.html',
  styleUrls: ['./list-blogs.component.sass']
})
export class ListBlogsComponent implements OnInit {

  faPlus = faPlus;
  faBlog = faBlog;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  url_images = environment.baseUrl;
  blogs:any[]=[];
  blogsAll:any[]=[];
  public page: number | undefined;

  constructor(private service: BlogsService,
              private loading: NgxSpinnerService,
              public serviceTranslation: TranslationService,) {
    this.getData();
  }

  ngOnInit(): void {
  }

  getData(){
    this.loading.show();
    this.service.getBlogs().subscribe( response => {
      this.blogs = response;
      this.blogsAll = response;
      console.log(response);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  active(blog: number){
    this.loading.show();
    this.service.activeBlogs(blog).subscribe( response => {
      const user = response;
      const index = this.blogs.findIndex(item => item.id == user.id);
      this.blogs[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(blog: number){
    this.loading.show();
    this.service.deleteBlogs(blog).subscribe( response => {
      const index = this.blogs.findIndex(item => item.id == response.id);
      this.blogs.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }



  filter(search: string){
    let regex = new RegExp(search, 'i');
    if (sessionStorage.getItem('language') == 'es'){
      this.blogs = this.blogsAll.filter(item => regex.test(item.title.es));
    } else {
      this.blogs = this.blogsAll.filter(item => regex.test(item.title.en));
    }
  }

}
