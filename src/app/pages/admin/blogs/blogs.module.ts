import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ComponentsModule} from '../../../components/components.module';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxDropzoneModule} from 'ngx-dropzone';
import {TranslateModule} from '@ngx-translate/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BlogsRoutingModule} from './blogs-routing.module';
import { ListBlogsComponent } from './list-blogs/list-blogs.component';
import { CreateBlogsComponent } from './create-blogs/create-blogs.component';
import { EditBlogsComponent } from './edit-blogs/edit-blogs.component';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [ListBlogsComponent, CreateBlogsComponent, EditBlogsComponent],
  imports: [
    CommonModule,
    BlogsRoutingModule,
    NgxSpinnerModule,
    ComponentsModule,
    CKEditorModule,
    NgxPaginationModule,
    NgxDropzoneModule,
    TranslateModule.forRoot(),
    FontAwesomeModule,
    FormsModule
  ]
})
export class BlogsModule { }
