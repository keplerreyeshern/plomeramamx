import { Component, OnInit } from '@angular/core';
import { faList, faBlog, faSave, faEye, faEyeSlash, faImages, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import {NgForm} from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {TranslationService} from '../../../../services/admin/translation.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {GalleriesService} from '../../../../services/admin/galleries.service';
import {BlogsService} from '../../../../services/admin/blogs.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-edit-blogs',
  templateUrl: './edit-blogs.component.html',
  styleUrls: ['./edit-blogs.component.sass']
})
export class EditBlogsComponent implements OnInit {

  faList = faList;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faImages = faImages;
  faBlog = faBlog;
  faSave = faSave;
  faEyeSlash = faEyeSlash;
  faEye = faEye;
  language = sessionStorage.getItem('language');
  url_images = environment.baseUrl;
  public Editor = ClassicEditor;
  image:any;
  imageInit:any;
  thumbnail:any;
  id:any;
  blog = {
    title: {
      es: '',
      en: ''
    },
    content: {
      es: '',
      en: ''
    },
    intro: {
      es: '',
      en: ''
    },
  };
  images:any[]=[];
  gallery: any;


  files: File[] = [];

  constructor(public serviceTranslation: TranslationService,
              private service: BlogsService,
              private loading: NgxSpinnerService,
              private router: Router,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.id = params['blog'];
      this.getBlog(params['blog']);
    });
  }

  getBlog(id: number){
    this.loading.show();
    this.service.showBlog(id).subscribe( response => {
      this.blog = response.blog;
      this.imageInit = response.blog.image;
      this.images = response.images;
      this.gallery = response.gallery;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }



  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit(form: NgForm) {
    this.loading.show();
    // @ts-ignore
    let language = this.language.toString();
    let filesLength = this.files.length.toString();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('content', form.value.content);
    params.append('intro', form.value.intro);
    if (this.image){
      params.append('image', this.image);
    }
    params.append('language', language);
    params.append('fileslength', filesLength);
    for (let f = 0; f < this.files.length; f++){
      params.append('file'+f, this.files[f]);
    }
    this.service.putBlogs(this.id, params).subscribe( response => {
      this.loading.hide();
      // console.log(response);
      this.router.navigateByUrl('/admin/blogs');
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 200){
        this.loading.hide();
        console.log(err);
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        console.log(err);
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

  deleteImage(gallery: number){
    this.loading.show();
    this.serviceImage.deleteImage(gallery).subscribe( response => {
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == response.id);
      this.images.splice(index, 1);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  activeImage(gallery: number){
    this.loading.show();
    this.serviceImage.activeImage(gallery).subscribe( response => {
      const user = response;
      // @ts-ignore
      const index = this.images.findIndex(item => item.id == user.id);
      this.images[index].active = response.active;
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
