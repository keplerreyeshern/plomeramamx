import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminComponent} from './admin.component';
import {AdminRoutingModule} from './admin-routing.module';
import {ComponentsModule} from '../../components/components.module';
import {TranslateModule} from '@ngx-translate/core';
import { UsersComponent } from './users/users.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component';
import { GalleriesComponent } from './galleries/galleries.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {DashboardComponent} from './dashboard/dashboard.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgxPaginationModule} from 'ngx-pagination';
import { BlogsComponent } from './blogs/blogs.component';
import { NewsComponent } from './news/news.component';
import {ChartsModule} from 'ng2-charts';



@NgModule({
    imports: [
        CommonModule,
        AdminRoutingModule,
        NgxSpinnerModule,
        ComponentsModule,
        CKEditorModule,
        NgxPaginationModule,
        NgxDropzoneModule,
        TranslateModule.forRoot(),
        ChartsModule
    ],
  declarations: [
    AdminComponent,
    UsersComponent,
    CategoriesComponent,
    ProductsComponent,
    GalleriesComponent,
    DashboardComponent,
    BlogsComponent,
    NewsComponent
  ]
})
export class AdminModule { }
