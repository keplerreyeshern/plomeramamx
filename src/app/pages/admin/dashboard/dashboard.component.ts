import {Component, Input, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Label, Color} from 'ng2-charts';
import {DashboardService} from '../../../services/admin/dashboard.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  aTelephone: any[]=[];
  aWhatsApp: any[]=[];
  accountantsT: any[]=[];
  accountantsW: any[]=[];

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public lineChartColors: Color[] = [
    { // Telephone
      backgroundColor: '#384859',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // WhatsApp
      backgroundColor: 'rgba(0,187,45)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
  ];
  public barChartLabels: Label[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: this.accountantsT, label: 'Telefono' },
    { data: this.accountantsW, label: 'WhatsApp' }
  ];

  constructor(private service: DashboardService,
              private loading: NgxSpinnerService) { }

  ngOnInit(): void {
    this.gatData();
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public gatData() {
    this.loading.show();
    this.service.getData().subscribe( response => {
      this.aTelephone = response.accountantsT;
      this.aWhatsApp = response.accountantsW;
      this.setData();
    }, err => {
      this.loading.hide();
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  setData(){
    if (this.accountantsT.length == 12){
      this.accountantsT = [];
    }
    if (this.accountantsW.length == 12){
      this.accountantsW = [];
    }
    for (let i=1; i<13; i++){
      let monthS = '';
      if (i > 9 ){
        monthS = i.toString();
      } else {
        monthS = '0' + i.toString();
      }
      let monthT = this.aTelephone.filter(item => item.month == monthS);
      let monthW = this.aWhatsApp.filter(item => item.month == monthS);

      this.accountantsT.push(monthT.length);
      this.accountantsW.push(monthW.length);

    }
    this.barChartData[0].data = this.accountantsT;
    this.barChartData[1].data = this.accountantsW;
    this.loading.hide();
  }


}
