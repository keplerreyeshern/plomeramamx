import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  storage = '';
  url = '';
  access_token = '';
  user: any;
  alert = false;
  alertAc = false;
  type = 'password';

  constructor(private spinnerService: NgxSpinnerService,
              private authService: AuthService,
              private router: Router) {}

  ngOnInit(): void {
    if(sessionStorage.getItem('access_token')){
      this.router.navigateByUrl('/admin');
    }
  }

  submit(form: NgForm){
    this.spinnerService.show();
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'NI4MIbQaIAb9a5UQB8tHA1tIvBGBSFoFsR1tENyH',
      username: form.value.email,
      password: form.value.password,
    };
    this.authService.postToken(params).subscribe( response => {
      // console.log(response);
      sessionStorage.setItem('token', JSON.stringify(response));
      this.storage = <string> sessionStorage.getItem('token');
      let start = this.storage.indexOf('access_token', 0);
      let substr = this.storage.substring(start);
      start = substr.indexOf(':', 0) + 2;
      substr = substr.substring(start);
      const end = substr.indexOf('"', 0);
      this.access_token = substr.substring(0, end );
      sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
      // console.log(sessionStorage.getItem('access_token'));
      this.access(sessionStorage.getItem('access_token'));
    }, err => {
      if(err.status == 400){
        this.spinnerService.hide();
        this.alert = true;
      } else if(err.status == 500){
        this.spinnerService.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 404){
        this.spinnerService.hide();
        alert('¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador');
      } else {
        this.spinnerService.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  access(params: any){
    this.authService.getDataUser(params).subscribe( response => {
      this.user = response;
      if (this.user.active){
        this.router.navigateByUrl('/admin');
        this.authService.signIn();
      } else {
        sessionStorage.clear();
        this.alertAc = true;
        this.authService.signOut();
      }
      this.spinnerService.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        this.spinnerService.hide();
      } else {
        alert('se detecto un error comunicate con el administrador');
        this.spinnerService.hide();
      }
    });
  }

}
