import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ComponentsService} from "../../services/public/components.service";
import {filter} from "rxjs/operators";
import {NavigationEnd, Router} from "@angular/router";

// @ts-ignore
declare var gtag;

@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.sass']
})
export class PublicComponent implements OnInit {

  constructor(private translateService: TranslateService,
              private service: ComponentsService,
              private router: Router) {
    const navEndEvents$ = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      );

    // @ts-ignore
    navEndEvents$.subscribe((event: NavigationEnd) => {
      gtag('config', 'G-EEFJS71XNZ', {
        'page_path': event.urlAfterRedirects
      });
    });
    sessionStorage.setItem('language', 'es');
  }

  ngOnInit(): void {
    let browserlang = this.translateService.getBrowserLang();
    if (sessionStorage.getItem('language') == 'es') {
      this.translateService.setDefaultLang(browserlang);
    } else {
      this.translateService.setDefaultLang('en');
    }
  }


  public useLanguage(lang: string): void {
    this.translateService.setDefaultLang(lang);
  }


  clickWP(type: string){
    this.service.setClick(type).subscribe( response => {
      console.log(response);
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
