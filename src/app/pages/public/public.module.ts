import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PublicRoutingModule} from './public-routing.module';
import {ComponentsModule} from '../../components/components.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {PublicComponent} from './public.component';
import { MainComponent } from './main/main.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {HttpClient} from '@angular/common/http';
import { PrivacyComponent } from './privacy/privacy.component';
import { ContactComponent } from './contact/contact.component';
import {RecaptchaModule} from 'ng-recaptcha';
import {FormsModule} from '@angular/forms';
import { AboutComponent } from './about/about.component';
import { ServicesComponent } from './services/services.component';
import {TruncatePipe} from "../../pipes/truncate.pipe";
import {ReleaseComponent} from "./release/release.component";
import {IvyGalleryModule} from 'angular-gallery';
import {FacebookModule} from "ngx-facebook";

export function TranslationLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}



@NgModule({
  imports: [
    CommonModule,
    PublicRoutingModule,
    NgxSpinnerModule,
    ComponentsModule,
    TranslateModule.forRoot({
      loader: {provide: TranslateLoader, useFactory: TranslationLoaderFactory, deps: [HttpClient]}
    }),
    RecaptchaModule,
    FormsModule,
    IvyGalleryModule,
    FacebookModule
  ],
  declarations: [PublicComponent, MainComponent, PrivacyComponent, ContactComponent, AboutComponent, ServicesComponent, ReleaseComponent, TruncatePipe]
})
export class PublicModule { }
