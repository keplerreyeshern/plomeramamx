import { Component, OnInit, ElementRef, AfterViewInit, Input } from '@angular/core';
import {TranslationService} from "../../../services/admin/translation.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ActivatedRoute, Router} from "@angular/router";
import {GalleriesService} from "../../../services/admin/galleries.service";
import {ReleaseService} from "../../../services/public/release.service";
import {environment} from "../../../../environments/environment";
import {Title, Meta } from "@angular/platform-browser";
import {Gallery} from 'angular-gallery';
import {FacebookService, InitParams, UIParams, UIResponse} from "ngx-facebook";

@Component({
  selector: 'app-release',
  templateUrl: './release.component.html',
  styleUrls: ['./release.component.sass']
})
export class ReleaseComponent implements OnInit {

  news = {
    title: {
      es: '',
      en: ''
    },
    content: {
      es: '',
      en: ''
    },
    intro: {
      es: '',
      en: ''
    },
    image: '',
    date: ''
  };
  galleryh:any;
  images:any;
  release = '';
  url_images = environment.baseUrl;
  url:string = '';
  titleName = '';

  constructor(public serviceTranslation: TranslationService,
              private service: ReleaseService,
              private loading: NgxSpinnerService,
              private router: Router,
              private serviceImage: GalleriesService,
              private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private meta: Meta,
              private gallery: Gallery,
              private fb: FacebookService) {

  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.release = params['release'];
      this.getData(params['release']);
      this.url = 'https://plomeramamx.com.mx/comunicados/' + this.release;
    });
    const initParams: InitParams = {
      appId: '307675644319584',
      xfbml: true,
      version: 'v10.0'
    };

    this.fb.init(initParams);

  }


  getData(release: string){
    this.loading.show();
    this.service.getRelease(release).subscribe( response => {
      this.news = response.news;
      this.galleryh = response.gallery;
      this.images = response.images;
      this.titleService.setTitle("Detalle del comunicado " + this.news.title.es);
      this.titleName = "Detalle del comunicado " + this.news.title.es;
      // this.meta.addTags([
      //   {property: "og:url", content: this.url},
      //   {property: "og:type", content:"Comunicado"},
      //   {property: "og:title", content: "Detalle del comunicado " + this.news.title.es},
      //   {property: "og:description", content: this.news.content.es},
      //   {property: "og:image", content: this.url_images + this.news.image}
      // ]);
      this.meta.updateTag({property: "og:url", content: this.url});
      this.meta.updateTag({property: "og:type", content:"Comunicado"});
      this.meta.updateTag({property: "og:title", content: "Detalle del comunicado " + this.news.title.es});
      this.meta.updateTag({property: "og:description", content: this.news.content.es});
      this.meta.updateTag({property: "og:image", content: this.url_images + this.news.image});
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
      this.loading.hide();
    });
  }

  showGallery(index: number) {
    let images:any[]=[];
    images.push({path: this.url_images + this.news.image})
    for(let i=0; i<this.images.length; i++){
      images.push({path: this.url_images + this.images[i].name})
    }
    let prop = {
      images: images,
      index
    };
    this.gallery.load(prop);
  }

  share(url: string) {

    const params: UIParams = {
      href: url,
      method: 'share',
      action_type: 'og.shares',
      action_properties: JSON.stringify({
        object : {
          'og:url': url, // your url to share
          'og:title': this.titleName,
          'og:site_name':'PlomeramaMX',
          'og:description':'description dshdkjshgdjkhsgdjkgsdgjhsdgskjhdgjkshdhkjsdsdhdshdhdhkdshd',
          'og:image': this.url_images + this.news.image,//
          'og:image:width':'250',//size of image in pixel
          'og:image:height':'257'
        }
      })
    };

    this.fb.ui(params)
      .then((res: UIResponse) => console.log(res))
      .catch((e: any) => console.error(e));

  }



}
