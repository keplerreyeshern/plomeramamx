import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';
import { MainComponent } from './main/main.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ContactComponent } from './contact/contact.component';
import {AboutComponent} from "./about/about.component";
import {ServicesComponent} from "./services/services.component";
import {ReleaseComponent} from "./release/release.component";

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        component: MainComponent
      },
      {
        path: 'nosotros',
        component: AboutComponent
      },
      {
        path: 'servicios',
        component: ServicesComponent
      },
      {
        path: 'contacto',
        component: ContactComponent
      },
      {
        path: 'comunicados/:release',
        component: ReleaseComponent
      },
      {
        path: 'aviso-privacidad',
        component: PrivacyComponent
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule { }
