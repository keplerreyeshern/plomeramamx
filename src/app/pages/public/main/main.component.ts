import { Component, OnInit } from '@angular/core';
import {MainService} from "../../../services/public/main.service";
import {NgxSpinnerService} from "ngx-spinner";
import {environment} from "../../../../environments/environment";
import {Meta, Title} from "@angular/platform-browser";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  news: any[]=[];
  galleries: any[]=[];
  images = environment.baseUrl;

  constructor(private service: MainService,
              private loading: NgxSpinnerService,
              private title: Title,
              private meta: Meta) {
    this.title.setTitle('Bienvenido a PlomeramaMx');
    this.meta.addTag({property: "og:url", content: "https://www.plomeramamx.com.mx"});
    this.meta.addTag({property: "og:type", content:"Plomeria y Electricidad"});
    this.meta.addTag({property: "og:title", content: "PlomeramaMX"});
    this.meta.addTag({property: "og:description", content: "Plomeria y Electricidad"});
    this.meta.addTag({property: "og:image", content: "https://plomeramamx.com.mx/assets/images/logo.png"});
  }

  ngOnInit(): void {
    this.getData();
  }


  getData(){
    this.loading.show();
    this.service.getData().subscribe( response => {
      this.news = response.news;
      this.galleries = response.galleries;
      // console.log(this.galleries);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
      this.loading.hide();
    });
  }

}
