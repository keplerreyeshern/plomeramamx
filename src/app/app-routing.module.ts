import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {LoginGuard} from './components/guards/gaurd/login.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/public/public.module').then( m => m.PublicModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin.module').then( m => m.AdminModule)
  },
  {
    path: 'contrasena',
    loadChildren: () => import('./pages/password/password.module').then( m => m.PasswordModule)
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
